import { HeartIcon } from '../sharedComponents'

const Dogs = ({ dogs, toggleLike, favorites }) => <div className="dogs-list">{dogs.map((dog, i) => {
    const urlArr = dog.url.split('.')
    const ext = urlArr[urlArr.length - 1]
    let media
    if (ext === "mp4" || ext === 'webm') media =  <video autoPlay>
        <source src={dog.url} type={`video/${ext}`} />
        Your browser does not support the video tag.
    </video>

    else media = <img alt={`dog number ${i}`} src={dog.url} />
    return <div key={dog.url} className="dog">{media}<HeartIcon onClick={() => toggleLike(i)} filled={favorites || dog.isFavorite} /></div>
    })}
</div>


export default Dogs