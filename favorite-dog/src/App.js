import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

import './App.css';
import Home from './pages/home'
import Favorites from './pages/favorites'

import {
  ROUTE_HOME,
  ROUTE_FAVORITES
} from './routes'

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="nav">
            <NavLink to={ROUTE_HOME} exact activeClassName="active" >Home</NavLink>
            <NavLink to={ROUTE_FAVORITES} exact activeClassName="active">Favorites</NavLink>
        </nav>
        <Switch>
          <Route path={ROUTE_FAVORITES}>
            <Favorites />
          </Route>
          <Route path={ROUTE_HOME}>
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
