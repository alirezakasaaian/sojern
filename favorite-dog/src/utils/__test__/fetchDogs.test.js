import fetchDogs from '../fetchDogs'
import { getStore, http } from '..'

jest.mock('..')

describe('fetchDogs', () => {
    it('should return list of 6 dogs none favorite', async () => {
        http.mockImplementation(() => Promise.resolve({url: "https://random.dog/example.jpg"}))
        getStore.mockImplementation(() => '{}')
        const res = await fetchDogs()
        
        expect(Object.keys(res)).toHaveLength(6)
        expect(res[0].isFavorite).toBe(false)
    })
    it('should return list of 6 dogs all favorite', async () => {
        http.mockImplementation(() => Promise.resolve({url: "https://random.dog/example.jpg"}))
        getStore.mockImplementation(() => ({"https://random.dog/example.jpg": {"url": "https://random.dog/example.jpg"}}))
        const res = await fetchDogs()

        expect(Object.keys(res)).toHaveLength(6)
        expect(res[0].isFavorite).toBe(true)
        expect(res[5].isFavorite).toBe(true)
    })
    it('should return list of 6 dogs all favorite except the first one', async () => {
        http.mockImplementationOnce(() => Promise.resolve({url: "https://random.dog/example1.jpg"}))
            .mockImplementation(() => Promise.resolve({url: "https://random.dog/example.jpg"}))
        getStore.mockImplementation(() => ({"https://random.dog/example.jpg": {"url": "https://random.dog/example.jpg"}}))
        const res = await fetchDogs()

        expect(Object.keys(res)).toHaveLength(6)
        expect(res[0].isFavorite).toBe(false)
        expect(res[5].isFavorite).toBe(true)
    })
})