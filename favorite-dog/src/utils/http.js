const http =  () => {
    return fetch('https://random.dog/woof.json').then(res => res.json())
}

export default http