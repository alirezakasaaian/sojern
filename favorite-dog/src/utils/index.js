export { default as http } from './http'
export { default as fetchDogs } from './fetchDogs'

export const getStore = () => localStorage.getItem('dogs') ? JSON.parse(localStorage.getItem('dogs')) : {}

// Function to Add or Remove items from store when they are toggled 
export const toggleOnStore = (item) => {
    const store = getStore()

    if(item.isFavorite) delete store[item.url]
    else store[item.url] = item
    localStorage.setItem('dogs', JSON.stringify(store))
}