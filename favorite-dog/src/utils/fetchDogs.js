import { http, getStore } from '.'

const fetchDogs = async () => {
    const requests = new Array(6)

    const store = getStore()
    for(let i =0; i < requests.length; i++){
        requests[i] = http('https://random.dog/woof.json').then(res => {
            res.isFavorite = !!store[res.url]
            return res
        })
    }
    const promise = await Promise.all(requests)
    return promise
}

export default fetchDogs