import { useState } from 'react'

import { HeartIcon, Dogs } from '../sharedComponents'
import { getStore } from '../utils'

const Favorites = () => {
    const store = getStore()
    const [dogs , setDogs] = useState(Object.values(store))
    
    const toggleLike = (i) => {
        const store = getStore()
        delete store[dogs[i].url]
        setDogs(Object.values(store))
        localStorage.setItem('dogs', JSON.stringify(store))
    }
    return (
        <>
            <h1>your <HeartIcon /> dogs </h1>
            <Dogs dogs={dogs} toggleLike={toggleLike} favorites />

        </>
    )
}

export default Favorites