import { useEffect, useState } from 'react'

import { fetchDogs, toggleOnStore } from '../utils'
import { Dogs } from '../sharedComponents'

const Home = () => {
    const [dogs , setDogs] = useState([])
    useEffect(() => {
        fetchDogs().then((res) => {
            setDogs(res)
        })
    }, [])

    const refresh = () => fetchDogs().then((res) => {
        setDogs(res)
    })

    const toggleLike = (i) => {
        const newArr = [...dogs]
        toggleOnStore(newArr[i])
        newArr[i].isFavorite = !newArr[i].isFavorite
        setDogs(newArr)
    }

    return (
        <>
            <h1>
                List of dogs
            </h1>
            <Dogs dogs={dogs} toggleLike={toggleLike} />
            <button className="button" onClick={refresh} >Refresh</button>
        </>
    )
}

export default Home