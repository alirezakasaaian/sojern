import { render, screen } from '@testing-library/react';
import Favorites from '../favorites';
import { getStore } from '../../utils'

jest.mock('../../utils')


test('renders 3 images from store', () => {
    const imgList = {"/example.jpg": {"url": "/example.jpg"}, "/example1.jpg": {"url": "/example1.jpg"}, "/example2.jpg": {"url": "/example2.jpg"}}
    getStore.mockImplementation(() => imgList)
    render(<Favorites />);
    const imgElements = screen.getAllByRole('img');
    expect(imgElements).toHaveLength(3);
});

test('renders 3 images from store and removes when like toggles', async () => {
    const imgList = {"/example.jpg": {"url": "/example.jpg"}, "/example1.jpg": {"url": "/example1.jpg"}, "/example2.jpg": {"url": "/example2.jpg"}}
    global.localStorage.setItem = jest.fn().mockImplementation(() => { delete imgList["/example.jpg"] })
    getStore.mockImplementation(() => imgList)
    render(<Favorites />);
    const imgElements = screen.getAllByRole('img');
    expect(imgElements).toHaveLength(3);
    const icons = screen.getAllByTestId('icon-like')

    icons[3].dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(Object.keys(imgList)).toHaveLength(2);

});
