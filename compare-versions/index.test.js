const compareVersions = require('.')

describe( "compareVersions", () => {
  describe("solid versions", () => {
    it( "should return 1 when version1 is higher", () => {
      expect( compareVersions( '3', '2' ) ).toBe( 1 );
    })
    it( "should return -1 when version2 is higher", () => {
      expect( compareVersions( '1', '2' ) ).toBe( -1 );
    })
    it( "should return 0 when version1 and version2 are equal", () => {
      expect( compareVersions( '3', '3' ) ).toBe( 0 );
    })
  })
  describe("nested versions equal length", () => {
    it( "should return 1 when version1 is higher when the last number is different", () => {
      expect( compareVersions( '1.1.2', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return 1 when version1 is higher when the first number is different", () => {
      expect( compareVersions( '2.1.1', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return 1 when version1 is higher when the number in the middle is different", () => {
      expect( compareVersions( '1.2.1', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return -1 when version2 is higher when the last number is different", () => {
      expect( compareVersions( '1.1.1', '1.1.2' ) ).toBe( -1 );
    })
    it( "should return -1 when version2 is higher when the first number is different", () => {
      expect( compareVersions( '1.1.1', '2.1.1' ) ).toBe( -1 );
    })
    it( "should return -1 when version2 is higher when the number in the middle is different", () => {
      expect( compareVersions( '1.1.1', '1.2.1' ) ).toBe( -1 );
    })
    it( "should return 0 when version1 and version2 are equal", () => {
      expect( compareVersions( '3.2.4', '3.2.4' ) ).toBe( 0 );
    })
  })
  describe("nested versions different length", () => {
    it( "should return 1 when version1 is higher when the last number is different", () => {
      expect( compareVersions( '1.1.1.1', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return 1 when version1 is higher when the first number is different", () => {
      expect( compareVersions( '2.1.1.1', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return 1 when version1 is higher when the number in the middle is different", () => {
      expect( compareVersions( '1.2.1.1', '1.1.1' ) ).toBe( 1 );
    })
    it( "should return -1 when version2 is higher when the last number is different", () => {
      expect( compareVersions( '1.1.1', '1.1.1.1' ) ).toBe( -1 );
    })
    it( "should return -1 when version2 is higher when the first number is different", () => {
      expect( compareVersions( '1.1.1', '2.1.1.1' ) ).toBe( -1 );
    })
    it( "should return -1 when version2 is higher when the number in the middle is different", () => {
      expect( compareVersions( '1.1.1', '1.2.1.1' ) ).toBe( -1 );
    })
  })
})