function compareVersions (v1, v2){
    const v1Arr = v1.split('.'),
    v2Arr = v2.split('.')
    const maxLength = v1Arr.length >= v2Arr.length ? v1Arr.length : v2Arr.length 
    for(let i = 0; i < maxLength; i++) {
        const v1Item = v1Arr[i] || 0,
        v2Item = v2Arr[i] || 0

        if(v1Item === v2Item) continue
        if(v1Item > v2Item) return 1
        if(v1Item < v2Item) return -1
    }
    return 0
}

module.exports = compareVersions